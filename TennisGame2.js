var TennisGame2 = function(player1Name, player2Name) {
    this.P1point = 0;
    this.P2point = 0;

    this.P1result = "";
    this.P2result = "";

    this.playerResults= {0:"Love",1:"Fifteen",2:"Thirty",3:"Forty"};

    this.player1Name = player1Name;
    this.player2Name = player2Name;
};

TennisGame2.prototype.getScore = function() {
    var score = "";

    score = this.GetResultInTheGame();
    return score;
};


TennisGame2.prototype.GetResultInTheGame = function() {

    var score = "";
    if (this.isATieAndPlayer1HasLessThanThreePoints()) {
        score = this.playerResults[this.P1point];
        score += "-All";
    }
    if (this.isADrawAndPlayer1HasMorethanTwoPoints()){
        score = "Deuce";
    }

    if ((this.isThePointsOfPlayer1GreaterThanPlayer2AndLessThanFour())||(this.isThePointsOfPlayer2GreaterThanPlayer1AndLessThanFour())){
        this.P1result = this.playerResults[this.P1point];
        this.P2result = this.playerResults[this.P2point];
        score = this.P1result + "-" + this.P2result;
    }
    if(this.isPlayer1OfPointsGreaterThanPlayer2OfPointsAndPlayer2OfGreaterThanOrEqualTo3(this.P1point,this.P2point)){
        score = "Advantage player1";
    }
    if(this.isPlayer1OfPointsGreaterThanPlayer2OfPointsAndPlayer2OfGreaterThanOrEqualTo3(this.P2point,this.P1point)){

        score = "Advantage player2";
    }
    if(this.isPlayer1OfPointsGreaterThanOrEqualTo4AndPlayer2OfPointsGreaterThanOrEqualToZeroAndTheDifferenceOfTheTwoPlayersOfPointsGreaterThanOrEqualTo2(this.P1point,this.P2point)) {
        score = "Win for player1";
    }
    if(this.isPlayer1OfPointsGreaterThanOrEqualTo4AndPlayer2OfPointsGreaterThanOrEqualToZeroAndTheDifferenceOfTheTwoPlayersOfPointsGreaterThanOrEqualTo2(this.P2point,this.P1point)) {
        score = "Win for player2";
    }
    return score;
};


TennisGame2.prototype.isATieAndPlayer1HasLessThanThreePoints = function(){

    return this.P1point === this.P2point && this.P1point < 3;
};


TennisGame2.prototype.isADrawAndPlayer1HasMorethanTwoPoints  = function(){

    return this.P1point === this.P2point && this.P1point > 2;
};

TennisGame2.prototype.isThePointsOfPlayer1GreaterThanPlayer2AndLessThanFour = function(){

    return (this.P1point > this.P2point && this.P1point < 4);

};

TennisGame2.prototype.isThePointsOfPlayer2GreaterThanPlayer1AndLessThanFour = function(){

    return (this.P2point > this.P1point && this.P2point < 4);

};

TennisGame2.prototype.isPlayer1OfPointsGreaterThanPlayer2OfPointsAndPlayer2OfGreaterThanOrEqualTo3 = function(pointPLayer1,pointPlayer2) {

    return (pointPLayer1 > pointPlayer2&& pointPlayer2 >= 3);

};

TennisGame2.prototype.isPlayer1OfPointsGreaterThanOrEqualTo4AndPlayer2OfPointsGreaterThanOrEqualToZeroAndTheDifferenceOfTheTwoPlayersOfPointsGreaterThanOrEqualTo2 = function(pointPLayer1,pointPlayer2){

    return (pointPLayer1 >= 4 && pointPlayer2 >= 0 && (pointPLayer1 - pointPlayer2) >= 2);
};




TennisGame2.prototype.SetP1Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P1Score();
    }
};

TennisGame2.prototype.SetP2Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P2Score();
    }
};

TennisGame2.prototype.P1Score = function() {
    this.P1point++;

};

TennisGame2.prototype.P2Score = function() {
    this.P2point++;
};

TennisGame2.prototype.wonPoint = function(player) {
    if (player === "player1")
        this.P1Score();
    else
        this.P2Score();
};

TennisGame2.prototype.getPoint1 = function() {
    return this.P1point;

};

if (typeof window === "undefined") {
    module.exports = TennisGame2;
}
